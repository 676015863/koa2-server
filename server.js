﻿const path = require("path");
const Koa = require("koa");
const helmet = require("koa-helmet");
const cors = require("koa-cors");
const responseTime = require("koa-response-time");
const body = require("koa-body");
const logger = require("koa-logger");
const fs = require("fs");
const koaStatic = require("koa-static");
const Router = require("koa-router");

const conf = {
  port: 5000
};

// 实例化
const app = new Koa();

app.use(logger());
app.use(responseTime());
app.use(helmet());

app.use(cors());
app.use(body({ multipart: true }));

// 设置路由
// 静态资源
app.use(
  koaStatic(path.join(__dirname, "static"), { maxage: 365 * 24 * 60 * 1000 })
);

app.use(
  new Router()
    .get("*", async (ctx, next) => {
      ctx.response.type = "html"; //指定content type
      ctx.response.body = await fs.createReadStream(
        path.join(__dirname, "index.html")
      );
    })
    .routes()
);

app.listen(conf.port, function() {
  console.log("服务器启动，监听 port： " + conf.port + "  running~");
});
